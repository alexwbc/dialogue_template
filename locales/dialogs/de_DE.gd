extends Node


var dialogue = [
     {
     "id"         : 1,
     "npc"        : "juliette",
     "q"          : "Haben nicht Heilge Lippen wie die Waller?",
     "a"          : "Ja, doch Gebet ist die Bestimmung aller",
     }
     ,{
     "id"         : 2,
     "npc"        : "juliette",
     "q"          : ["O so vergцnne, teure Heilge nun, Daя auch die Lippen wie die Hдnde tun.","O so vergцnne, teure Heilge nun, Daя auch die Lippen wie die Hдnde tun.\nVoll Inbrunst beten sie zu dir: erhцre, Daя Glaube nicht sich in Verzweiflung kehre!"],
     "a"          : "Du weiяt, ein Heilger pflegt sich nicht zu regen, Auch wenn er eine Bitte zugesteht.",
     }
     ,{
     "id"         : 3,
     "npc"        : "juliette",
     "q"          : ["[Chance zu küssen: 50%] So reg dich, Holde, nicht, wie Heilge pflegen, Derweil mein Mund dir nimmt, was er erfleht","So reg dich, Holde, nicht, wie Heilge pflegen, Derweil mein Mund dir nimmt, was er erfleht\nNun hat dein Mund ihn aller Sьnd entbunden."],
     "a"          : "So hat mein Mund zum Lohn Sьnd fьr die Gunst?",
     }
     ,{
     "id"         : 4,
     "npc"        : "juliette",
     "q"          : ["Chance zu küssen: 100%] Zum Lohn die Sьnd?  O Vorwurf, sья erfunden!","Zum Lohn die Sьnd?  O Vorwurf, sья erfunden!\nGebt sie zurьck!"],
     "cinematic"  : [
          {"speaker":"juliette","text":"Ihr kьяt recht nach der Kunst."},
          {"speaker":"nurse","text":"Mama will Euch ein Wцrtchen sagen, Frдulein."},
          {"speaker":"player","text":"Wer ist des Frдuleins Mutter?"},
          {"speaker":"nurse","text":"Ei nun, Junker, Das ist die gnдdge Frau vom Hause hier, Gar eine wackre Frau und klug und ehrsam. Die Tochter, die Ihr spracht, hab ich gesдugt.\nIch sag Euch, wer ihr' habhaft werden kann,Ist wohl gebettet."},
          {"speaker":"player","text":"  Sie eine Capulet?  O teurer Preis!  Mein Leben Ist meinem Feind als Schuld dahingegeben!"}],

     }
]

