extends Node
var test = "test"

var dialogue = [
     {
     "id"         : 1,
     "npc"        : "juliette",
     "q"          : "Les saintes n’ont-elles pas des lèvres, et les pèlerins aussi?",
     "a"          : "Oui, pèlerin, des lèvres vouées à la prière.",
     }
     ,{
     "id"         : 2,
     "npc"        : "juliette",
     "q"          : ["Oh ! alors, chère sainte, que les lèvres fassent ce que font les mains","Oh ! alors, chère sainte, que les lèvres fassent ce que font les mains\nElles te prient ; exauce-les, de peur que leur foi ne se change en désespoir."],
     "a"          : "Les saintes restent immobiles, tout en exauçant les prières.",
     }
     ,{
     "id"         : 3,
     "npc"        : "juliette",
     "q"          : ["[chance d'embrasser: 50%] Restez donc immobile, tandis que je recueillerai l’effet de ma prière","Restez donc immobile, tandis que je recueillerai l’effet de ma prière\nVos lèvres ont effacé le péché des miennes."],
     "a"          : "Mes lèvres ont gardé pour elles le péché qu’elles ont pris des vôtres",
     }
     ,{
     "id"         : 4,
     "npc"        : "juliette",
     "q"          : ["[chance d'embrasser: 100%] Vous avez pris le péché de mes lèvres ? Ô reproche charmant !","Vous avez pris le péché de mes lèvres ? Ô reproche charmant !\nAlors rendez-moi mon péché"],
     "cinematic"  : [
          {"speaker":"juliette","text":"Vous avez l’art des baisers"},
          {"speaker":"nurse","text":"Madame, votre mère voudrait vous dire un mot"},
          {"speaker":"player","text":"Qui donc est sa mère?"},
          {"speaker":"nurse","text":"Eh bien, bachelier sa mère est la maîtresse de la maison, une bonne dame, et sage et vertueuse\nje vais vous dire : celui qui parviendra à mettre la main sur elle pourra faire sonner les écus."},
          {"speaker":"player","text":"C’est une Capulet ! ô trop chère créance! Ma vie est due à mon ennemie!"}],

     }
]

func get(stuff):
	return "adasds"
