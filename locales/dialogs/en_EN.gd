extends Node

#dictionary values:
#id/npc_id:	combination of these two, makes an unique id (ie: [03]and [Tom] is the 3rd dialogue line for npc "Tom"
#
#Q:		question (also text selecion)
#A:		NPC answer if this question is picked
var dialogue = [
#		TOM lines

     {
     "id"         : 0,#									internal ID for Tom dialogues
     "npc"        : "tom",#								npc who belong this line
     "q"          : "Hello Tom, what's your name?",#				player dialogue
     "a"          : ["Did...did you just said my name asking for it?", "becouse that's crazy, dude"],#npc answer (multiple lines)
     "cinematic"  : [
          {"speaker":"player","text":"if you think that's crazy, wait to see my next questions"},
          {"speaker":"tom","text":"what's wrong with your next question?"}],
    
     "enabled"    : true,#								if the line is enabled from new game
     "activate"   : [1,2],#	picking this line enable other lines (relative to current npc)
     "disable"    : null,#other lines to be disable other lines (the picked one is always disabled)
     }
     ,{
     "id"         : 1,
     "npc"        : "tom",
     "q"          : ["Aww... come on...","Aww... come on...you can tell me, Tom.. what' your name, don't be shy.. tell, tell me"],
     "a"          : ["I... I... jeez, I don't even know what to say. It's Tom... TOM, OK?", "and I still don't know you"],
     "enabled"    : false,
     "activate"   : 3,
     "disable"    : 2,
     }
     ,{
     "id"         : 2,
     "npc"        : "tom",
     "q"          : ["Yeh, relax dude.. I am just asking"],
     "a"          : "Didn't you really see what's wrong here? I mean, you can't be THAT clueless",
     "enabled"    : false,
     "disable"    : 1,
     }
     ,{
     "id"         : 3,
     "npc"        : "tom",
     "q"          : ["Here we are, TOM...","Here we are, \"Tomok\"; wasn't that hard, was it?"],
     "a"          : "Tom...what? ahh, nevermind. Listen buddy, it's \"Tom\". Just Tom...\nand in case you were wondering...\"just\" is NOT included as well",
     "enabled"    : false,
     }
     ,{
     "id"         : 4,
     "npc"        : "tom",
     "q"          : ["this line will shown if eithier event [rain] or [cloudy] are true"],
     "a"          : "so, basically you can't see the sun",
     "enabled"    : false,
     "inclusive"  : ["rain", "cloudy"],
     "activate"   : 3,
     "disable"    : 1,
     }
     ,{
     "id"         : 5,
     "npc"        : "tom",
     "q"          : ["this line will shown ONLY if event [elections] is set true"],
     "a"          : "becouse it doesn't make any sense to chat about candidates otherwise",
     "enabled"    : false,
     "exclusive"  : ["elections"],
     "activate"   : 3,
     "disable"    : 1,
     }
     ,{
     "id"         : 6,
     "npc"        : "tom",
     "q"          : ["lorem ipsum"],
     "a"          : "ipsum indeed",
     "enabled"    : false,
     "inclusive"  : "rain",
     "activate"   : 3,
     "disable"    : 1,
     }

#		Sarah lines
     ,{
     "id"         : 0,
     "npc"        : "sarah",
     "q"          : ["Hello Sarah"],
     "a"          : ["Oh, hello... whorever you are"],
     "enabled"    : true,
     "activate"   : 1,
     "disable"    : 0,
     }
     ,{
     "id"         : 1,
     "npc"        : "sarah",
     "q"          : ["I am a friend"],
     "a"          : ["Do I have to belive you?"],
     "enabled"    : false,
     "activate"   : 2,
     "disable"    : 0,
     }
     ,{
     "id"         : 0,
     "npc"        : "demo",
     "q"          : "This is my first question",
     "a"          : "theen... have your first answer!",
     "enabled"    : true,
     "activate"   : 1,
     }
     ,{
     "id"         : 1,
     "npc"        : "demo",
     "q"          : "Can't you tell me a bit more?",
     "a"          : ["sure, as your npc I can reply you back with lot of stuff","See? I am talking again! just be sure to put something like this:","[\"one array of answer in sequence\",\"separated by comma\"]"],
     "enabled"    : false,
     "activate"   : 2,
     }
     ,{
     "id"         : 2,
     "npc"        : "demo",
     "q"          : "Cool! but can I pick among multiple questions?",
     "a"          : ["...of course, you just need to set the proper \"activate\" array","the first two option activated the ones with id 1 and this one (2)","but now with the code \n\"activate\":[3,4,5,6,7]\nyou'll have lot of more options!", "check this out..."],
     "enabled"    : false,
     "activate"   : [3,4,5,6,7],
     }
     ,{
     "id"         : 3,
     "npc"        : "demo",
     "q"          : "Great, I'll take the one with \"3\" ID",
     "a"          : ["Good choice!\nbut now we will disable the other one with\n\"disable\":[4,5,6,7]","don't forget the one you pick will be disabled, so we don't need to use \"disable\" for every line"],
     "enabled"    : false,
     "activate"   : 8,
     "disable"    : [4,5,6,7],
     }
     ,{
     "id"         : 4,
     "npc"        : "demo",
     "q"          : "The \"4\" looks nice, I'll pick that one",
     "a"          : ["Good choice!\nbut now we will disable the other one with\n\"disable\":[3,5,6,7]","don't forget the one you pick will be disabled, so we don't need to use \"disable\" for every line"],
     "enabled"    : false,
     "activate"   : 8,
     "disable"    : [3,5,6,7],
     }
     ,{
     "id"         : 5,
     "npc"        : "demo",
     "q"          : "The fifth it's my favorite one",
     "a"          : ["Good choice!\nbut now we will disable the other one with\n\"disable\":[3,4,6,7]","don't forget the one you pick will be disabled, so we don't need to use \"disable\" for every line"],
     "enabled"    : false,
     "activate"   : 8,
     "disable"    : [3,4,6,7],
     }
     ,{
     "id"         : 6,
     "npc"        : "demo",
     "q"          : "The six sounds nicier. I go for that one",
     "a"          : ["Good choice!\nbut now we will disable the other one with\n\"disable\":[3,4,5,7]","don't forget the one you pick will be disabled, so we don't need to use \"disable\" for every line"],
     "enabled"    : false,
     "activate"   : 8,
     "disable"    : [3,4,5,7],
     }
     ,{
     "id"         : 7,
     "npc"        : "demo",
     "q"          : "Seven is the biggest one so far? Looks like it will be my pick",
     "a"          : ["Good choice!\nbut now we will disable the other one with\n\"disable\":[3,4,5,6]","don't forget the one you pick will be disabled, so we don't need to use \"disable\" for every line"],
     "enabled"    : false,
     "activate"   : 8,
     "disable"    : [3,4,5,6],
     }
     ,{
     "id"         : 8,
     "npc"        : "demo",
     "q"          : "What? Everytime I select one it get disabled? What if I want to keep it?",
     "a"          : ["you just need to enable the current one,\nie: this one has id \"8\", so you only need\n\"activate\":8","this is now an infinite loop, welcome in the twilight zone!"],
     "activate"   : 8,
     "enabled"    : false,
     }
     ,{
     "id"         : 1,
     "npc"        : "juliette",
     "q"          : "Have not saints lips, and holy palmers too?",
     "a"          : "Ay, pilgrim, lips that they must use in pray'r",
     "activate"   : 2,
     "enabled"    : true,
     }
     ,{
     "id"         : 2,
     "npc"        : "juliette",
     "q"          : ["O, then, dear saint, let lips do what hands do!","O, then, dear saint, let lips do what hands do!\nThey pray; grant thou, lest faith turn to despair."],
     "a"          : "Saints do not move, though grant for prayers' sake",
     "activate"   : 3,
     "enabled"    : false,
     }
     ,{
     "id"         : 3,
     "npc"        : "juliette",
     "q"          : ["[kiss: chance 50%] Then move not while my prayer's effect I take.","Then move not while my prayer's effect I take.\nThus from my lips, by thine my sin is purg'd."],
     "a"          : "Then have my lips the sin that they have took.",
     "activate"   : 4,
     "enabled"    : false,
     }
     ,{
     "id"         : 4,
     "npc"        : "juliette",
     "q"          : ["[kiss: chance 100%] Sin from my lips? O trespass sweetly urg'd!","Sin from my lips? O trespass sweetly urg'd!\nGive me my sin again."],
     "activate"   : 1,
     "enabled"    : false,
     "cinematic"  : [
          {"speaker":"juliette","text":"You kiss by th' book."},
          {"speaker":"nurse","text":"Madam, your mother craves a word with you."},
          {"speaker":"player","text":"What is her mother?"},
          {"speaker":"nurse","text":"Marry, bachelor, Her mother is the lady of the house. And a good lady, and a wise and virtuous.\nI nurs'd her daughter that you talk'd withal. I tell you, he that can lay hold of her shall have the chinks."},
          {"speaker":"player","text":" Is she a Capulet? O dear account! my life is my foe's debt."}],

     }
]





func newgame():
	get_node("/root/database").dialogue = dialogue
	queue_free()

func refreshdb():#refresh the database with this translation
	var database = get_node("/root/database")
	for i in range(dialogue.size()):
		database.dialogue[i]["q"] = dialogue[i]["q"]
		database.dialogue[i]["a"] = dialogue[i]["a"]
	queue_free()
