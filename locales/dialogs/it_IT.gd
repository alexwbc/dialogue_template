extends Node


var dialogue = [
     {
     "id"         : 1,
     "npc"        : "juliette",
     "q"          : "Santi e palmieri non han dunque labbra?",
     "a"          : "Sì, pellegrino, ma quelle son labbra  ch’essi debbono usar per la preghiera. ",
     }
     ,{
     "id"         : 2,
     "npc"        : "juliette",
     "q"          : ["E allora, cara santa, che le labbra facciano anch’esse quel che fan le mani","E allora, cara santa, che le labbra facciano anch’esse quel che fan le mani\n esse sono in preghiera innanzi a te, ascoltale, se non vuoi che la fede volga in disperazione."],
     "a"          : "I santi, pur se accolgono i voti di chi prega, non si muovono. ",
     }
     ,{
     "id"         : 3,
     "npc"        : "juliette",
     "q"          : ["[possibilità di bacio: 50%] E allora non ti muovere fin ch’io raccolga dalle labbra tue","E allora non ti muovere fin ch’io raccolga dalle labbra tue  l’accoglimento della mia preghiera.\nEcco, dalle tue labbra ora le mie purgate son così del lor peccato. "],
     "a"          : "Ma allora sulle mie resta il peccato di cui si son purgate quelle tue!",
     }
     ,{
     "id"         : 4,
     "npc"        : "juliette",
     "q"          : ["[possibilità di bacio: 100%] O colpa dolcemente rinfacciata!","O colpa dolcemente rinfacciata!\nIl mio peccato succhiato da te! E rendimelo, allora, il mio peccato. "],
     "cinematic"  : [
          {"speaker":"juliette","text":"Sai baciare nel più perfetto stile"},
          {"speaker":"nurse","text":"Tua madre vuol parlarti, padroncina. "},
          {"speaker":"player","text":"Chi è sua madre?"},
          {"speaker":"nurse","text":"Ebbene, giovanotto, è la padrona qui di questa casa; una buona signora, saggia e onesta;\ne la figliola, quella damigella con cui discorrevate poco fa, gliel’ho allattata ed allevata io. E quell’uomo che saprà fare tanto da prenderla per moglie, giuraddio, ne avrà dei bei sonanti quattrinelli!"},
          {"speaker":"player","text":"Ella è una Capuleti!... Ah, duro prezzo ch’io sarò tratto a pagare per questo! Do in pegno la mia vita a una nemica!"}],

     }
]

